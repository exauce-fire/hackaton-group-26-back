# Start from the latest Node.js base image
FROM node:latest

# Set the Current Working Directory inside the container
WORKDIR /app

# Update system packages and install Go
RUN apt-get update && apt-get install -y golang-go

# Set up environment variables for Go
ENV GOPATH=/root/go
ENV PATH=$PATH:$GOPATH/bin

COPY go.mod go.sum ./
# Copy everything from the host to the current working directory inside the container
COPY . .

COPY ./.env.dist ./.env

# Install the necessary Go packages
RUN go get golang.org/x/text/transform \
    && go get golang.org/x/text/unicode/norm \
    && go get github.com/swaggo/swag \
    && go install github.com/swaggo/swag/cmd/swag@latest

# Initialize Swaggo
RUN swag init --parseDependency --parseInternal

# Install the necessary npm packages
RUN npm install @marp-team/marp-core \
    && npm install markdown-it-include --save \
    && npm install markdown-it-container --save \
    && npm install markdown-it-attrs --save

# You may specify the command to run your app using CMD or ENTRYPOINT depending on your use case
CMD ["go", "run", "./main.go"]

EXPOSE 8000